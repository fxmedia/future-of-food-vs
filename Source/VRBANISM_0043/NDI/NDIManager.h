// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "NDIManager.generated.h"

UCLASS()
class VRBANISM_0043_API ANDIManager : public AActor
{
	GENERATED_BODY()
	
public:	 // Public functions
	// Sets default values for this actor's properties
	ANDIManager();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
