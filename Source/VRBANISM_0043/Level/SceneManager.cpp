// Fill out your copyright notice in the Description page of Project Settings.


#include "SceneManager.h"

// Sets default values
ASceneManager::ASceneManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASceneManager::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASceneManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASceneManager::UpdateScene(FReplyLiveLocationData data)
{
	UE_LOG(LogTemp, Log, TEXT("ASceneManager::UpdateScene Updating visibility of hosts and speakers"));

	for (ANDIReceiveActor* NDIReceiver : HostsAndSpeakers) {
		if (data.active_scenes.Contains(NDIReceiver->Name)) {
			NDIReceiver->SetActorHiddenInGame(false);
		}
		else {
			NDIReceiver->SetActorHiddenInGame(true);
		}
	}
}