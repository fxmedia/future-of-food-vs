// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/StaticMeshComponent.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SeatsManager.generated.h"

UCLASS()
class VRBANISM_0043_API ASeatsManager : public AActor
{
	GENERATED_BODY()

public:  // Public variables
	// C++ Root Component
	UPROPERTY(BlueprintReadWrite, Category = Seats)
	USceneComponent* RootSceneComponent;

	// Rows mapped to row number
	UPROPERTY(BlueprintReadWrite, Category = Seats)
	TMap<int32, USceneComponent*> Rows;

	// Seat availability mapped to the corresponding seat
	UPROPERTY(BlueprintReadWrite, Category = Seats)
	TMap<USceneComponent*, bool> isSpawnPointTaken;

	// Minimal amount of visible rows
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Seats)
	int32 MinimalRows;

	// Total amount of rows detected in the blueprint
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Seats)
	int32 DetectedRows;

	// Current amount of visible rows
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Seats)
	int32 CurrentRows;

	// Height offset of invitee spawn location above the seat spawn point
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Seats)
	float SpawnHeightOffset;

	// Rotation offset, around the Z-axis, of invitee spawn location
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Seats)
	float SpawnRotationOffset;

	// Random offset in the XY axis, set to 0 for no offset
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Random)
	float RandomSpawnRangeXY = 5.0f;

	// Random offset in the Z axis, set to 0 for no offset
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Random)
	float RandomSpawnRangeZ = 5.0f;

public:  // Public functions
	// Sets default values for this actor's properties
	ASeatsManager();

	// Get the next available empty seat world position
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	FTransform GetEmptySeat();

	// Get the next available empty seat world position
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	bool HasEmptySeat();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};