// Fill out your copyright notice in the Description page of Project Settings.


#include "PollObject.h"

// Sets default values
APollObject::APollObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void APollObject::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APollObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APollObject::InitPoll_Implementation(FReplyLivePollData PollData)
{
	UE_LOG(LogTemp, Log, TEXT("APollObject::InitPoll Executing in C++"));
}

void APollObject::UpdatePoll_Implementation(FReplyLiveAPIPollAnswers PollAnswers)
{
	UE_LOG(LogTemp, Log, TEXT("APollObject::InitPoll Executing in C++"));
}

void APollObject::ExitPoll_Implementation(FReplyLivePollData PollData, bool KeepVisibile)
{
	UE_LOG(LogTemp, Log, TEXT("APollObject::ExitPoll Executing in C++"));
}