// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "CoreMinimal.h"
#include "InviteeTextWidget.generated.h"

UCLASS()
class VRBANISM_0043_API UInviteeTextWidget : public UUserWidget
{
	GENERATED_BODY()

public:  // Public functions
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void SetText(const FString & Text);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void SetName(const FString & FullName);
	
};
