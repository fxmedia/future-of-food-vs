// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CarouselObject.generated.h"

UCLASS()
class VRBANISM_0043_API ACarouselObject : public AActor
{
	GENERATED_BODY()

public:  // Public variables
	// Name used by reply.live to identify the object
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Carousel)
	FString Name;

public:	 // Public functions
	// Sets default values for this actor's properties
	ACarouselObject();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void ShowCarouselObject();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void HideCarouselObject();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
