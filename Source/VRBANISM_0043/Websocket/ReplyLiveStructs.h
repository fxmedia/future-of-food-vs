// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ReplyLiveStructs.generated.h"


/*
 General Reply.live structures 
 */
USTRUCT(BlueprintType)
struct FReplyLiveMessage
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString channel;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString event;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString data;
};

/*
Poll Reply.live structures

Structs are ordered from leaf to root, due to dependencies
*/
USTRUCT(BlueprintType)
struct FReplyLiveAnswerOptionAttributes
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString answer;
};

USTRUCT(BlueprintType)
struct FReplyLiveAnswer
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString answer_key;
};

USTRUCT(BlueprintType)
struct FReplyLiveAnswerOption
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString key;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString layout;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FReplyLiveAnswerOptionAttributes attributes;
};

USTRUCT(BlueprintType)
struct FReplyLiveModule
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 module_id;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString status;
};

USTRUCT(BlueprintType)
struct FReplyLivePollData
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 id;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString question;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FReplyLiveAnswerOption> answer_options;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FReplyLiveAnswer> answers;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FReplyLiveModule active_module;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 sort_order;
};

USTRUCT(BlueprintType)
struct FReplyLivePollMessage
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString channel;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString event;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FReplyLivePollData data;
};

/*
Location Reply.live structures

Structs are ordered from leaf to root, due to dependencies
*/
USTRUCT(BlueprintType)
struct FReplyLiveCameraMotion
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString row_id;
};

USTRUCT(BlueprintType)
struct FReplyLiveScene
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString row_id;
};

USTRUCT(BlueprintType)
struct FReplyLiveCamera
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString row_id;
};

USTRUCT(BlueprintType)
struct FReplyLiveLocationData
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 id;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString name;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FReplyLiveCamera> cameras;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FReplyLiveScene> scenes;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FReplyLiveCameraMotion> camera_motions;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString active_camera;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString active_motion;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FString> active_scenes;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString active_simluation;
};

USTRUCT(BlueprintType)
struct FReplyLiveLocationMessage
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString channel;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString event;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FReplyLiveLocationData data;
};

/*
CarouselObject Reply.live structures

Structs are ordered from leaf to root, due to dependencies
*/
USTRUCT(BlueprintType)
struct FReplyLiveObjectData
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString name;
};


USTRUCT(BlueprintType)
struct FReplyLiveObjectMessage
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString channel;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString event;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FReplyLiveObjectData data;
};

/*
Post Reply.live structures

Structs are ordered from leaf to root, due to dependencies
*/
USTRUCT(BlueprintType)
struct FReplyLiveInviteeGroups
{
	GENERATED_BODY()

	// TODO: Fill with key-value pairs once they are available
};

USTRUCT(BlueprintType)
struct FReplyLiveInvitee
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 id;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString firstname;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString lastname;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString avatar;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString attending_status;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool muted;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 is_moderator;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool has_joined_virtual_studio;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FReplyLiveInviteeGroups> groups;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 online_count;
};

USTRUCT(BlueprintType)
struct FReplyLivePostData
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 id;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 chatroom_id;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FReplyLiveInvitee invitee;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 invitee_id;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString invitee_name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString message;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString media;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString emoji;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool visible;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool highlight;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString created_at;
};

USTRUCT(BlueprintType)
struct FReplyLivePostMessage
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString channel;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString event;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FReplyLivePostData data;
};

/*
Chatroom Reply.live structures

Structs are ordered from leaf to root, due to dependencies
*/
USTRUCT(BlueprintType)
struct FReplyLiveChatroomData
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString token;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString topic;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FReplyLivePostData> posts;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool allow_media;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool visible_by_default;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool only_own_posts;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool active;
};

USTRUCT(BlueprintType)
struct FReplyLiveChatroomMessage
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString channel;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString event;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FReplyLiveChatroomData data;
};

/*
Invitee Reply.live structures

Structs are ordered from leaf to root, due to dependencies
*/
USTRUCT(BlueprintType)
struct FReplyLiveInviteeMessage
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString channel;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString event;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FReplyLiveInvitee data;
};

/*
API invitees joined Reply.live structures

Structs are ordered from leaf to root, due to dependencies
*/
USTRUCT(BlueprintType)
struct FReplyLiveAPIInviteesJoined
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FReplyLiveInvitee> invitees;
};

/*
API poll answers Reply.live structures

Structs are ordered from leaf to root, due to dependencies
*/
USTRUCT(BlueprintType)
struct FReplyLiveAPIPollAnswer
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 id;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 invitee_id;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString answer_key;
};

USTRUCT(BlueprintType)
struct FReplyLiveAPIPollAnswers
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FReplyLiveAPIPollAnswer> poll_answers;
};